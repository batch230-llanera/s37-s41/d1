const User = require("../models/User.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNumber: reqBody.mobileNumber,
		password: bcrypt.hashSync(reqBody.password, 10)
		// 10 -salt

	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false
		}
	}

	)
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			// compareSync is a bcrypt function to compare unhased password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// true or false

			if(isPasswordCorrect){
				// Let's give the user a token to access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;
			}
		}
	})
}

/*
// s38
	module.exports.getProfile = (reqBody) => {
	return User.findOne({_id: reqBody._id}).then(result => {
		if(result == null){
			return false
		}
		else{
			result.password = ""
			return result
		}
	})
}

*/


/*
s38 Activity Solution
Solution 1 - password display ("******")
modules.exports.getProfile = (reqBody) => {
	return User.findById(reqBody._id).then((result, err)=>{
	if(err){
		return false;
	}
	else{
		result.password = "*****";
		return result;
	}
	})
}

*/

//Solution 2
module.exports.getProfile = (request, response) => {

	// will contain your decoded token
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "*****";
		response.send(result);
	})
}

// enroll feature
module.exports.enroll = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	let courseName = await Course.findById(request.body.courseId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header (request header contains the user token)
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		courseId: request.body.courseId,
		courseName: courseName
	}

	console.log(newData)

	let isUserUpdated = await User.findById(newData.userId).then(user => {
		user.enrollments.push({
			courseId: newData.courseId,
			courseName: newData.courseName
		})

		return user.save().then(result =>{
		console.log(result);
		return true;
		})
		.catch(error => {
		console.log(error);
		return false;
		})

	})
	console.log(isUserUpdated)

	let isCourseUpdated = await Course.findById(newData.courseId).then(course => {

		course.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
	
		// Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a response that a user is not allowed to enroll due to no available slots
		// [3] Else if the slot is negative value, it should make the slot value to zero

		// Minus the slots available by 1
		// course.slots = course.slots - 1;

		if(course.slots <= 0){
			console.log ("Unable to enroll. No slots available")
			return false
		}
		else{
			course.slots -=1;

			return course.save()
			.then(result => {
				console.log(result);
				return true
			})
			.catch(error => {
				console.log(error);
				return false
			})
		}
		
	})

	console.log(isCourseUpdated)

	// Condition will check if both "user" and "course" document has been updated
	// Ternary operator
	// (isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false)

	
		if(isUserUpdated == true && isCourseUpdated == true){
			response.send(true);
		}
		else{
			response.send(false);
		}
	

}