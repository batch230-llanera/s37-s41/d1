const express = require("express");
const router = express.Router()
const userController = require("../controllers/userControllers.js")
const auth = require("../auth.js");

router.post("/register", (request, response) => {
	userController.registerUser(request.body)
	.then(resultFromController => response.send(resultFromController))
})

router.post("/checkEmail", (request, response) => {
	userController.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

/*
	router.post("/details", (request, response) => {
	userController.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})
*/

/*
function loginUser(reqBody){}
loginUser(request.body)
*/

/*
s38 Activity Solution
router.post("/details", (request, response) => {
	userController.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})

*/
//Solution 2
router.get("/details", auth.verify, userController.getProfile);

router.post("/enroll", auth.verify, userController.enroll)

module.exports = router;